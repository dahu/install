# Script for testing Collatex with sample data. Generate a dot output and then an svg output

# Download Collatex
#https://collatex.net/download/
wget https://oss.sonatype.org/service/local/repositories/releases/content/eu/interedition/collatex-tools/1.7.1/collatex-tools-1.7.1.jar -O collatex-tools-1.7.1.jar
mkdir -p input output

INPUT_JSON="input/example.json"
OUTPUT="output/example"


echo "INPUT"
cat $INPUT_JSON
echo "-----"
CMD="java -jar collatex-tools-1.7.1.jar $INPUT_JSON  -f dot -o $OUTPUT.dot"
echo -e "Collatex command: \e[33m$CMD\e[0m"
$CMD
echo "-----"
echo "DOT OUPUT"
cat ${OUTPUT}".dot"
echo "-----"
CMD="dot ${OUTPUT}.dot -Tsvg > ${OUTPUT}.svg"
echo -e "dot to svg command: \e[33m$CMD\e[0m"
dot ${OUTPUT}.dot -Tsvg > ${OUTPUT}.svg
echo "-----"
echo "SVG OUPUT"
eog ${OUTPUT}".svg"
echo "-----"
