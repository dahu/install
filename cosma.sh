# See https://github.com/graphlab-fr/cosma and https://cosma.arthurperret.fr/installing.html
# Need NodeJS - See https://nodejs.org/en - Choice: install via snap, see: https://github.com/nodejs/snap
sudo snap install node --classic
node --version # Check if ok

# Install C*sma
sudo npm install @graphlab-fr/cosma -g
sudo npm update cosma -g

# Installing an example - See: https://cosma.arthurperret.fr/user-manual.html#configuration
mkdir ~/Tools/cosma_example
cd ~/Tools/cosma_example/

# Config from scratch
cosma config
gedit config.yml # Edit and modify the following (example) values:
## files_origin: "./data"
## export_target: "./export/"
mkdir ./data
mkdir ./export/

# Create a first record in interactive mode using the following line
# cosma record
## choose title, type and tags (for ex. cosmopolitain / cocktail / alcoholic) and check the result in gedit data/${title}.md (data/cosmopolitain.md in our example)

# Create a record in one line
cosma autorecord "Cosmopolitain" "cocktail" "alcoholic"
cosma autorecord "Margarita" "cocktail" "alcoholic, red"
cosma autorecord "Virgin Mojito" "cocktail" "soft"
## Type and tags values must before be declared in the config file (config.yml). Tags values are comma-separated values
## Les valeurs de type et d'étiquettes doivent être déclarées préalablement dans le fichier de configuration. Les étiquettes doivent être séparées par des virgules

# Create the graph and view it
cosma modelize
firefox export/cosmoscope.html

# Documentation: https://hyperotlet.github.io/cosma/fr.html
# Exemple de graphe assez complet et qui parle d'HN : https://www.arthurperret.fr/digithum-glossaire-hn.html
