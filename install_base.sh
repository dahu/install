# Generic needs on linux 
## Install
sudo apt update && sudo apt upgrade
sudo apt install vlc default-jre git pdftk libxml2-utils xsltproc php mysql-server php-mysql php-mbstring gimp tree vim emacs tesseract-ocr tesseract-ocr-* chromium-browser openvpn network-manager-openvpn network-manager-openvpn-gnome

## Remove not useful stuffs
sudo apt remove thunderbird gnome-mines gnome-mahjongg gnome-sudoku gnome-todo rhythmbox

## From downloaded files (08/01/2024)
sudo dpkg -i ./download/code_1.85.1-1702462158_amd64.deb
sh ./download/oxygen-64bit-openjdk.sh
