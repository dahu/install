# Source: https://computingforgeeks.com/how-to-install-r-and-rstudio-on-ubuntu-debian-mint/?utm_content=cmp-true
sudo apt-get install r-base
wget https://download1.rstudio.org/electron/jammy/amd64/rstudio-2023.06.1-524-amd64.deb
sudo apt install -f ./rstudio-2023.06.1-524-amd64.deb
sudo apt-get install libxml2-dev #useful for library("stemmatology")
