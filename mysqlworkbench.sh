# Cf. https://dev.mysql.com/doc/mysql-apt-repo-quick-guide/en/
# Download from https://dev.mysql.com/downloads/repo/apt/
# Direct link for Linux Ubuntu	
wget https://dev.mysql.com/get/mysql-apt-config_0.8.29-1_all.deb ~/Téléchargements/
sudo dpkg -i ~/Téléchargements/mysql-apt-config_0.8.29-1_all.deb
# Which MySQL product do you wish to configure?
# Laisser tel quel et choisir "Ok"
sudo apt-get update
sudo apt-get install mysql-server
systemctl status mysql # Fermer avec CTL+C
sudo apt-get install mysql-workbench-community # Laisser : Use strong password

## Si pb de dépendances non satisfaites
# apt --fix-broken install
